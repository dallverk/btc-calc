import {
  fetch,
} from '../utils/dataAccess';

export function error(error) {
  return { type: 'BTC_INDEX_SHOW_ERROR', error };
}

export function loading(loading) {
  return { type: 'BTC_INDEX_SHOW_LOADING', loading };
}

export function success(retrieved) {
  return { type: 'BTC_INDEX_SHOW_SUCCESS', retrieved };
}

export function btcIndex() {
  return dispatch => {
    dispatch(loading(true));
    dispatch(error(''));

    const path = 'v1/bpi/currentprice.json';

    fetch(path)
      .then(response =>
        response
          .json()
          .then(retrieved => ({ retrieved }))
      )
      .then(({ retrieved }) => {
        dispatch(loading(false));
        dispatch(success(retrieved));
      })
      .catch(e => {
        dispatch(loading(false));
        dispatch(error(e.message));
      });
  };
}

export function reset(eventSource) {
  return dispatch => {
    if (eventSource) eventSource.close();

    dispatch({ type: 'BTC_INDEX_SHOW_RESET' });
  };
}
