import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { CurrencyField } from '../calc';

const defaultProps = {
  code: 'USD',
  amount: 0,
  rate_float: 0,
};

test('CurrencyField uses onChange', () => {
  const handleChange = jest.fn();
  render(<CurrencyField {...defaultProps} onChange={handleChange} onRemove={() => {}} />);
  const inputEl = screen.getByTestId('currency-input');
  fireEvent.change(inputEl, {target: { value: '1' }});
  expect(handleChange).toHaveBeenCalledTimes(1);
});

test('CurrencyField uses onRemove', () => {
  const handleRemove = jest.fn();
  render(<CurrencyField {...defaultProps} onChange={() => {}} onRemove={handleRemove} />);
  const buttonEl = screen.getByTestId('currency-remove');
  fireEvent.click(buttonEl, {});
  expect(handleRemove).toHaveBeenCalledTimes(1);
});
