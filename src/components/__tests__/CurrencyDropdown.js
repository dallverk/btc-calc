import React from 'react';
import { render } from '@testing-library/react';
import { CurrencyDropdown } from '../calc';

test('CurrencyDropdown should render', () => {
  render(<CurrencyDropdown onChange={() => {}} options={[]} />);
});
