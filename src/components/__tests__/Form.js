import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Form } from '../calc';

const defaultProps = {
  bpi: {},
  btc: '0',
  currencies: [],
};

test('Form BTC field uses onChange', () => {
  const handleBtcChange = jest.fn();
  render(<Form {...defaultProps} onBtcChange={handleBtcChange} onCurrencyChange={() => {}} />);
  const inputEl = screen.getByTestId('btc-input');
  fireEvent.change(inputEl, {target: { value: '1' }});
  expect(handleBtcChange).toHaveBeenCalledTimes(1);
});
