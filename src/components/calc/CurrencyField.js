import React from 'react';
import PropTypes from 'prop-types';
import { IconButton, TextField, Grid } from '@material-ui/core';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';

import { calcBpiValue, DEFAULT_LOCALE } from './Form';

export const FIELD_WIDTH = 200;

export function formatValue(value, code = 'USD') {
  const options = { style: 'currency', currency: code };
  const formatSum = new Intl.NumberFormat(DEFAULT_LOCALE, options).format(value || 0);
  return formatSum;
}

export default function CurrencyField({ code, amount, rate_float, onChange, onRemove, ...props }) {
  const [value, setValue] = React.useState('');
  const textInput = React.useRef(null);

  React.useEffect(() => {
    if (textInput && document.activeElement !== textInput.current) {
      const bpiValue = calcBpiValue(amount, { code, rate_float });
      setValue(formatValue(bpiValue, code));
    }
  }, [amount, code, rate_float, textInput]);

  function handleChange({ target }) {
    setValue(target.value);
    if (onChange) {
      onChange({ newValue: target.value, rate_float });
    }
  }

  function handleRemoveClick() {
    if (onRemove) {
      onRemove({ code });
    }
  }

  return (
    <Grid container alignItems="center" spacing={1} {...props}>
      <Grid item>
        <TextField
          label={code}
          variant="filled"
          inputRef={textInput}
          value={value}
          onChange={handleChange}
          inputProps={{ 'data-testid': 'currency-input' }}
          style={{ width: FIELD_WIDTH }}
        />
      </Grid>
      <Grid item>
        <IconButton data-testid="currency-remove" onClick={handleRemoveClick}>
          <RemoveCircleOutlineIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
}

CurrencyField.propTypes = {
  code: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  rate_float: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
};
