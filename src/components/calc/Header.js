import React from 'react';
import PropTypes from 'prop-types';

export default function Header({ chartName, disclaimer, time, intervalUpdated, ...props }) {
  const { updated } = time || {};
  const intervalUpdatedStr = intervalUpdated && intervalUpdated.toTimeString();
  return (
    <div {...props}>
      <h1>{chartName}</h1>
      <p>{disclaimer}</p>
      {updated && <p>Index by: {updated}</p>}
      {intervalUpdatedStr && <small>Last updated: {intervalUpdatedStr}</small>}
    </div>
  );
}

Header.propTypes = {
  // Coindesk data
  chartName: PropTypes.string.isRequired,
  disclaimer: PropTypes.string.isRequired,
  time: PropTypes.object.isRequired,
  // Component interval update
  intervalUpdated: PropTypes.instanceOf(Date),
};
