import React from 'react';
import PropTypes from 'prop-types';

export default function Error({ error }) {

  return (
    <h1>{error}</h1>
  );
}

Error.propTypes = {
  error: PropTypes.string.isRequired,
};
