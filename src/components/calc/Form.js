import React from 'react';
import PropTypes from 'prop-types';
import { TextField, Grid } from '@material-ui/core';

import CurrencyField from './CurrencyField';
import CurrencyDropdown from './CurrencyDropdown';

export const DEFAULT_LOCALE = 'en-US';

export function formatValue(value) {
  const options = { style: 'decimal' };
  const formatSum = new Intl.NumberFormat(DEFAULT_LOCALE, options).format(value || 0);
  return formatSum;
}

export function calcBpiValue(value, bpi) {
  const { rate_float } = bpi || {};
  return value * rate_float;
}

export function calcCurrValue(value, bpi) {
  const { rate_float } = bpi || {};
  return value / rate_float;
}

export function validate(value) {
  if (isNaN(value)) {
    return 'Please enter numeric values only';
  } else if (value < 0) {
    return 'The value cannot be negative';
  } else {
    return null;
  }
}

export function prepareAmount(value, valueError) {
  const floatVal = String(value).replace(/,/g, '');
  if (valueError !== null) {
    return 0;
  } else {
    return Number(floatVal);
  }
}

export default function Form({ bpi, btc, onBtcChange, currencies, onCurrencyChange, ...props }) {
  const [valueError, setValueError] = React.useState(null);

  function handleValueChange({ target }) {
    const validateResult = validate(target.value);
    setValueError(validateResult);
    if (onBtcChange) {
      onBtcChange(target.value);
    }
  }

  function handleCurrencyFieldChange({ newValue, rate_float }) {
    const validateResult = validate(newValue);
    setValueError(validateResult);
    if (validateResult === null) {
      const newCalcValue = calcCurrValue(newValue, { rate_float });
      if (onBtcChange) {
        onBtcChange(formatValue(newCalcValue));
      }
    }
  }

  function handleCurrencyFieldRemove({ code }) {
    const newFields = currencies.filter(item => item.code !== code);
    if (onCurrencyChange) {
      onCurrencyChange(newFields)
    }
  }

  function handleCurrencyDropdownChange({ code }) {
    const bpiCurrencies = Object.values(bpi || {});
    if (currencies.findIndex(item => item.code === code) === -1) {
      const newFields = currencies.filter(item => item.code !== code);
      const newFieldIndex = bpiCurrencies.findIndex(item => item.code === code);
      if (newFieldIndex !== -1) {
        newFields.push(bpiCurrencies[newFieldIndex]);
      }
      if (onCurrencyChange) {
        onCurrencyChange(newFields);
      }  
    }
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={3}>
        <TextField
          label="BTC"
          id="btc-input"
          variant="filled"
          value={btc}
          onChange={handleValueChange}
          error={!!valueError}
          helperText={valueError}
          inputProps={{ 'data-testid': 'btc-input' }}
        />
      </Grid>
      <Grid item xs={12} sm={9}>
        <Grid container spacing={2}>
          {currencies && currencies.map((value) => (
            <Grid item xs={12} key={value.code}>
              <CurrencyField
                onRemove={handleCurrencyFieldRemove}
                onChange={handleCurrencyFieldChange}
                amount={prepareAmount(btc, valueError)}
                {...value}
              />
            </Grid>
          ))}
          <Grid item xs={12}>
            {currencies && currencies.length < Object.values(bpi || {}).length && (
              <CurrencyDropdown onChange={handleCurrencyDropdownChange} options={Object.values(bpi || {})} />
            )}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

Form.propTypes = {
  // BPI Object from Coindesk with currencies
  bpi: PropTypes.object.isRequired,
  // BTC amount
  btc: PropTypes.string.isRequired,
  onBtcChange: PropTypes.func.isRequired,
  // Currencies with calculated rates. Can be removed or added.
  currencies: PropTypes.array.isRequired,
  onCurrencyChange: PropTypes.func.isRequired,
};
