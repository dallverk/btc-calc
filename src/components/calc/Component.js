import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Grid } from '@material-ui/core';

import { btcIndex as retrieveBtcIndex } from '../../actions/btcIndex';

import Form from './Form';
import Header from './Header';
import Error from './Error';

export const DELAY = 60000;

class BtcCalc extends React.Component {
  static propTypes = {
    retrieved: PropTypes.object,
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    retrieveBtcIndex: PropTypes.func.isRequired,
  };

  state = {
    date: new Date(),
    btcAmount: '',
    currencies: [],
  };

  componentDidMount() {
    this.interval = setInterval(this.tick, DELAY);
    this.props.retrieveBtcIndex();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.retrieved !== this.props.retrieved && !!this.props.retrieved) {
      const { bpi } = this.props.retrieved || {};
      const bpiCurrencies = bpi && Object.values(bpi);
      if (prevState.currencies.length === 0) {
        this.setState({
          currencies: bpiCurrencies,
        });
      } else {
        const newFields = bpiCurrencies.filter(item => prevState.currencies.some(curr => item.code === curr.code));
        this.setState({
          currencies: newFields,
        });
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick = () => {
    this.props.retrieveBtcIndex();
    this.setState({
      date: new Date(),
    });
  };

  handleBtcChange = (value) => {
    this.setState({
      btcAmount: value,
    });
  };

  handleCurrencyChange = (selected) => {
    this.setState({
      currencies: selected,
    });
  };

  render() {
    const { loading, error, retrieved } = this.props;
    return (
      <Container maxWidth="md">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            {!!retrieved && <Header intervalUpdated={this.state.date} {...retrieved} />}
          </Grid>
          <Grid item xs={12}>
            {error && !loading && <Error error={error}  />}
          </Grid>
          <Grid item xs={12}>
            {!!retrieved && (
              <Form
                onCurrencyChange={this.handleCurrencyChange}
                currencies={this.state.currencies}
                onBtcChange={this.handleBtcChange}
                btc={this.state.btcAmount}
                {...retrieved}
              />
            )}
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  retrieved: state.btcIndex.show.retrieved,
  error: state.btcIndex.show.error,
  loading: state.btcIndex.show.loading,
});

const mapDispatchToProps = dispatch => ({
  retrieveBtcIndex: () => dispatch(retrieveBtcIndex()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BtcCalc);
