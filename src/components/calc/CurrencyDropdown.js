import React from 'react';
import PropTypes from 'prop-types';
import { Select, MenuItem } from '@material-ui/core';

import { FIELD_WIDTH } from './CurrencyField';

export default function CurrencyDropdown({ options, onChange, ...props }) {
  function handleChange({ target }) {
    if (onChange) {
      onChange({ code: target.value });
    }
  }

  return (
    <Select
      variant="outlined"
      displayEmpty
      onChange={handleChange}
      value={''}
      renderValue={() => {
        return <em>Add Currency</em>;
      }}
      style={{ width: FIELD_WIDTH }}
      {...props}
    >
      <MenuItem disabled value="">
        <em>Add Currency</em>
      </MenuItem>
      {options && options.map((value) => (
        <MenuItem key={value.code} value={value.code}>
          {value.code}
        </MenuItem>
      ))}
    </Select>
  );
}

CurrencyDropdown.propTypes = {
  options: PropTypes.array,
  onChange: PropTypes.func.isRequired,
};
